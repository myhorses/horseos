# HorseOS

Descompromissed, hobbyist and fun operating system.  

Destined to research and tests, it should not be used in production environments.  

![myhorse.png](https://gitlab.com/myhorses/horseos/-/raw/main/Arts/myhorse.png)

See information to obtain here:  
https://gitlab.com/myhorses/horseos/-/blob/main/docs/obtain.md

This is provided "AS IS", without warranty of any kind.  

Based on Debian GNU/Linux.  

---

All programs contained in the distribution follow their respective licenses established by their respective authors.  

The scripts autored by HorseOS team are licensed under MIT license.  

---

Copyright (c) 2023-2025 Raymond Risko and Renato Ferreira
