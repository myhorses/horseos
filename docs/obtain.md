# HorseOS

Descompromissed, hobbyist and fun operating system.  

Destined to research and tests, it should not be used in production environments.  

![myhorse.png](https://gitlab.com/myhorses/horseos/-/raw/main/Arts/myhorse.png)

This is provided "AS IS", without warranty of any kind.  

HorseOS is based on Debian GNU/Linux.  

---

## HorseOS 10.13 XFCE i686

![replace-print.png](https://gitlab.com/myhorses/horseos/-/raw/main/Arts/myhorse.png)

Link to download:  
https://mega.io/

MD5SUM: asdfg  

---

All programs contained in the distribution follow their respective licenses established by their respective authors.  

The scripts autored by HorseOS team are licensed under MIT license.  

---

Copyright (c) 2023 Raymond Risko and Renato Ferreira